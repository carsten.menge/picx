// ----------------------------------------------------------------------------
// db.go contains the database logic
// TODO:
// ----------------------------------------------------------------------------

package main

import (
	"errors"
	"fmt"
	"io"
	"net/http"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// ----------------------------------------------------------------------------
// global constants for database access
const (
	dbName               = "HA19DB-Carsten-Menge-630145"
	dbAddress            = "localhost:27017"
	userCollection       = "userCollection"
	motiveCollCollection = "motiveCollCollection"
	motivePicCollection  = "motivePicCollection"
	poolCollection       = "poolCollection"
	poolPicCollection    = "poolPicCollection"
	mosaicCollection     = "mosaicCollection"
)

// connect to the main database
func connectDB() *mgo.Database {

	s, err := mgo.Dial(dbAddress)
	if err != nil {
		errorHandler(err)
	}
	db := s.DB(dbName)

	return db
}

// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// function to save files to the users files gridFS. Returns slice of ObjectIds
// for the files in the GridFS
func saveToGridFs(w http.ResponseWriter, r *http.Request) ([]bson.ObjectId, error) {

	var idList []bson.ObjectId

	// set a high enough size limit for multiple picture upload
	err := r.ParseMultipartForm(32000000)
	if err != nil {
		return nil, errors.New(err.Error())
	}

	// loop through the files
	for _, header := range r.MultipartForm.File["fileUpload"] {

		if checkImageFormat(header.Filename) {

			// open the file
			file, err := header.Open()
			if err != nil {
				return nil, errors.New(err.Error())
			}
			defer file.Close()

			// write the contents into a new gridFile
			gridFile, err := gridfs.Create(header.Filename)
			if err != nil {
				return nil, errors.New(err.Error())
			}
			defer gridFile.Close()

			_, err = io.Copy(gridFile, file)
			if err != nil {
				return nil, errors.New(err.Error())
			}

			idList = append(idList, gridFile.Id().(bson.ObjectId))
		} else {

			fmt.Printf("file %s is not in a supported image format", header.Filename)
		}

	}

	return idList, nil
}

// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// database operations

// get a pool by ID
func getPool(poolID bson.ObjectId) pool {

	pool := pool{}
	db.C(poolCollection).FindId(poolID).One(&pool)
	return pool
}

// get a motiveCollection by ID
func getMotiveColl(motiveCollID bson.ObjectId) motiveColl {

	motiveColl := motiveColl{}
	db.C(motiveCollCollection).FindId(motiveCollID).One(&motiveColl)
	return motiveColl
}

// get the user's pools as a slice
func getUserPools(userID string) []pool {

	pools := []pool{}

	user := getUser(userID)

	for _, poolID := range user.PoolIDs {
		pools = append(pools, getPool(poolID))
	}

	return pools
}

// get the user's motive collections as a slice
func getUserMotiveColls(userID string) []motiveColl {

	motiveColl := []motiveColl{}

	user := getUser(userID)

	for _, motiveCollID := range user.MotiveCollsIDs {
		motiveColl = append(motiveColl, getMotiveColl(motiveCollID))
	}

	return motiveColl
}

// get a user from a pool id
func getUserFromPoolID(poolID bson.ObjectId) user {

	pool := pool{}

	db.C(poolCollection).FindId(poolID).One(&pool)
	return getUser(pool.UserID.Hex())
}

// get a user from a motiveColl id
func getUserFromMotiveCollID(motiveCollID bson.ObjectId) user {

	motiveColl := motiveColl{}

	db.C(motiveCollCollection).FindId(motiveCollID).One(&motiveColl)
	return getUser(motiveColl.UserID.Hex())
}

// get a motiveColl from a motive id
func getMotiveCollFromMotiveID(motivePicID bson.ObjectId) motiveColl {

	motivePic := motivePic{}

	db.C(motivePicCollection).FindId(motivePicID).One(&motivePic)
	return getMotiveColl(motivePic.CollectionID)
}

// get a pool from a poolPic
func getPoolFromPoolPicID(poolPicID bson.ObjectId) pool {

	pool := pool{}

	db.C(poolPicCollection).FindId(poolPicID).One(&pool)
	return pool
}

// get a poolPic by ID
func getPoolPicByID(poolPicID bson.ObjectId) poolPic {

	poolPic := poolPic{}

	db.C(poolPicCollection).FindId(poolPicID).One(&poolPic)
	return poolPic
}

// get a motive by ID
func getMotivePicByID(motiveID bson.ObjectId) motivePic {

	motivePic := motivePic{}

	db.C(motivePicCollection).FindId(motiveID).One(&motivePic)
	return motivePic
}

// add a new motives collection to the user
func addMotivesCollection(userID string, motiveCollName string) {

	// add to the motives collection collection, with a generated new ID
	id := bson.NewObjectId()

	motiveColl := motiveColl{}
	motiveColl.ID = id
	motiveColl.IDHex = id.Hex()
	motiveColl.Name = motiveCollName
	motiveColl.UserID = bson.ObjectIdHex(userID)

	err := db.C(motiveCollCollection).Insert(motiveColl)
	if err != nil {
		errorHandler(err)
	}

	// add the generated ID to the user's collection
	push := bson.M{"$push": bson.M{"MotiveCollsIDs": id}}
	err = db.C(userCollection).UpdateId(bson.ObjectIdHex(userID), push)
	if err != nil {
		errorHandler(err)
	}
}

// add a new pool to a user
func addPool(userID string, poolName string, size int) bson.ObjectId {

	// add to the pools collection, with a generated new ID
	id := bson.NewObjectId()

	pool := pool{}
	pool.ID = id
	pool.IDHex = id.Hex()
	pool.Name = poolName
	pool.Size = size
	pool.UserID = bson.ObjectIdHex(userID)

	err := db.C(poolCollection).Insert(pool)
	if err != nil {
		errorHandler(err)
	}

	// add the generated ID to the user's collection
	push := bson.M{"$push": bson.M{"PoolIDs": id}}
	err = db.C(userCollection).UpdateId(bson.ObjectIdHex(userID), push)
	if err != nil {
		errorHandler(err)
	}

	return id
}

// add a new picture to a pool
func addToPool(size int, poolID bson.ObjectId, fileID bson.ObjectId) {

	newPoolPic := poolPic{}
	newPoolPic.FileID = fileID
	newPoolPic.Processed = false
	newPoolPic.MetaDataGenerated = false
	newPoolPic.ID = bson.NewObjectId()
	newPoolPic.CollectionID = poolID
	newPoolPic.Size = size
	// we don't like empty ObjectID fields...
	newPoolPic.MetaData.GradientFileID = bson.NewObjectId()

	err := db.C(poolPicCollection).Insert(newPoolPic)
	if err != nil {
		errorHandler(err)
	}

	push := bson.M{"$push": bson.M{"PicsIDs": newPoolPic.ID}}
	err = db.C(poolCollection).UpdateId(poolID, push)
	if err != nil {
		errorHandler(err)
	}

	poolPicResizeChan <- newPoolPic
}

// get a mosaic by ID
func getMosaic(mosaicID bson.ObjectId) mosaicPic {

	var mosaic mosaicPic
	err := db.C(mosaicCollection).FindId(mosaicID).One(&mosaic)
	if err != nil {
		errorHandler(err)
	}

	return mosaic
}

// add a new picture to a motives collection
func addToMotivesCollection(name string, motiveCollID bson.ObjectId, fileID string) {

	newMotivePic := motivePic{}
	newMotivePic.ID = bson.NewObjectId()
	newMotivePic.IDHex = newMotivePic.ID.Hex()
	newMotivePic.Name = name
	newMotivePic.CollectionID = motiveCollID
	newMotivePic.FileID = bson.ObjectIdHex(fileID)
	newMotivePic.ThumbnailGenerated = false
	newMotivePic.MetaDataGenerated = false
	newMotivePic.ThumbnailID = bson.NewObjectId()
	newMotivePic.MetaData.GradientFileID = bson.NewObjectId()
	// TODO: mgo doesn't like empty ObjectID fields... fix only by db refactor?

	err := db.C(motivePicCollection).Insert(newMotivePic)
	if err != nil {
		errorHandler(err)
	}

	push := bson.M{"$push": bson.M{"MotivePicsIDs": newMotivePic.ID}}
	db.C(motiveCollCollection).UpdateId(motiveCollID, push)

	motiveThumbnailChan <- newMotivePic
}

// get a user object
func getUser(userID string) user {

	user := user{}
	db.C(userCollection).FindId(bson.ObjectIdHex(userID)).One(&user)

	return user
}

// replace an image reference for a user and delete the old file
func replacePoolPic(oldPic poolPic, newPic poolPic) {

	// add new poolPic
	newPic.CollectionID = oldPic.CollectionID
	db.C(poolPicCollection).Insert(newPic)

	pool := getPool(oldPic.CollectionID)
	// add newPic to collection
	pool.PicsIDs = append(pool.PicsIDs, newPic.ID)
	// remove the oldPic from collection
	pool.PicsIDs = removeFromPicsIDs(pool.PicsIDs, oldPic.ID)

	// update the pool and poolPicColl
	db.C(poolPicCollection).RemoveId(oldPic.ID)
	db.C(poolCollection).UpdateId(pool.ID, pool)

	// remove the file from the GridFS
	gridfs.RemoveId(oldPic.FileID)
}

// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// helper functions

// remove idToRemove from ids
func removeFromPicsIDs(ids []bson.ObjectId, idToRemove bson.ObjectId) []bson.ObjectId {

	var index int
	found := false
	// find the index
	for i, id := range ids {
		if id == idToRemove {
			found = true
			index = i
			break
		}
	}
	if found {
		ids[index] = ids[len(ids)-1]
		ids = ids[:len(ids)-1]
	}

	return ids
}

// ----------------------------------------------------------------------------
