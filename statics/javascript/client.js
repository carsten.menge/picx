// ----------------------------------------------------------------------------
// Clientside javascript to access picx
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// get function to access api. Returns JSON-responses (as Promises)
function get(url, queryKey, queryValue) {

  return new Promise(function (resolve, reject) {

    var xhr = new XMLHttpRequest();
    xhr.open("GET", url + "?" + queryKey + "=" + queryValue)

    xhr.onload = function () {

      if (this.status >= 200 && this.status < 300) {

        let res = JSON.parse(xhr.response);
        resolve(res);
      } else {
        reject({
          status: this.status,
          statusText: xhr.statusText
        });
      }

    };

    xhr.onerror = function () {
      reject({
        status: this.status,
        statusText: xhr.statusText
      });
    };

    xhr.send();

  });
}
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// functions to populate the client with data
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// populate the motive options
function populateMotiveOptions(userID) {

  let select = document.getElementById("mosaicMotivePicID")

  while (select.firstChild) {
    select.removeChild(select.firstChild)
  }

  get("/picx/getMotiveColls", "userID", userID).then(colls => {

    colls.forEach(coll => {

      let optgrp = document.createElement("optgroup")
      optgrp.setAttribute("label", coll.Name)
      select.appendChild(optgrp)

      coll.MotivePics.forEach(motivePic => {
        let opt = document.createElement("option")
        opt.setAttribute("value", motivePic.ID)
        let text = document.createTextNode(motivePic.Name)
        opt.appendChild(text)
        optgrp.appendChild(opt)
      });
    });

  });
}
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// populate the mosaic browser
function populateMosaics(userID) {

  parentDiv = document.getElementById("browseMosaicsDiv")
  while (parentDiv.firstChild) {
    parentDiv.removeChild(parentDiv.firstChild)
  }

  get("/picx/getMosaics", "userID", userID).then(mosaics => {

    mosaics.forEach(mosaic => {
      // call the helper function
      displayMosaic(mosaic, parentDiv)
    })

  })
}
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// get the pool images and populate the pool browser
function showPool() {

  let parentDiv = document.getElementById("poolPics");

  while (parentDiv.firstChild) {
    parentDiv.removeChild(parentDiv.firstChild)
  }

  let s = document.getElementById("poolsInsp");
  let poolID = s.options[s.selectedIndex].value;

  get("/picx/getPool", "poolID", poolID).then(pool => {

    let gradientDiv = document.createElement("div")
    gradientDiv.classList.add("gradientDiv")

    let gradImg = document.createElement("img")
    let gradUrl = "/picx/getPoolGradient?poolID=" + pool.ID
    gradImg.setAttribute("src", gradUrl)

    let p = document.createElement("p")
    p.innerHTML = "Poolname: " + pool.Name + ", w/h: " + pool.Size + "px";

    gradientDiv.appendChild(p)
    gradientDiv.appendChild(gradImg)
    parentDiv.appendChild(gradientDiv)

    pool.PicsIDs.forEach(picID => {

      get("/picx/getPoolPic", "poolPicID", picID).then(pic => {

        let img = document.createElement("img");
        img.setAttribute("src", "/picx/getImage?id=" + pic.FileID)

        parentDiv.appendChild(img)
      });
    });

  });
}
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// get the motives and populate the motive browser
function showMotives() {

  let parentDiv = document.getElementById("motivePics");

  while (parentDiv.firstChild) {
    parentDiv.removeChild(parentDiv.firstChild)
  }

  let s = document.getElementById("motiveInsp");
  let motiveCollID = s.options[s.selectedIndex].value;

  get("/picx/getMotiveColl", "motiveCollID", motiveCollID).then(motiveColl => {

    motiveColl.MotivePicsIDs.forEach(picID => {

      get("/picx/getMotive", "motiveID", picID).then(pic => {

        let div = document.createElement("div")
        let a = document.createElement("a")
        let url = "/picx/getImage?id=" + pic.FileID
        let p = document.createElement("p")
        p.classList.add("motiveMeta")
        div.classList.add("motivePicDiv")
        a.setAttribute("href", url)

        gradImg = document.createElement("img")
        gradImg.classList.add("gradImg")
        gradUrl = "/picx/getImage?id=" + pic.MetaData.GradientFileID
        gradImg.setAttribute("src", gradUrl)

        let img = document.createElement("img");
        img.setAttribute("src", "/picx/getImage?id=" + pic.ThumbnailID)

        p.innerHTML = "h: " + pic.MetaData.Height + "px\nw: " + pic.MetaData.Width + "px"

        a.appendChild(img)
        div.appendChild(a)
        div.appendChild(p)
        div.appendChild(gradImg)
        parentDiv.appendChild(div)
      });
    });

  });
}
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// helper function to display the noiseLevel as text
function changeSlider() {

  let slider = document.getElementById("noiseLevelSlider");
  let txtEl = document.getElementById("noiseLevelText");

  txtEl.innerHTML = slider.value;
}
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
// helper function to display mosaic pictures
function displayMosaic(mosaic) {

  if (mosaic.Done) {
    div = document.createElement("div")
    div.setAttribute("class", "pictureDiv")

    a = document.createElement("a")
    img = document.createElement("img")
    url = "/picx/getImage?id=" + mosaic.FileID
    a.setAttribute("href", url)
    img.setAttribute("src", url)

    div.appendChild(a)
    a.appendChild(img)
    parentDiv.appendChild(div)
  }
}
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// Button listeners
// ----------------------------------------------------------------------------

function newPoolClick() {

  document.getElementById("newPoolFormDiv").classList.remove("hidden");
  document.getElementById("uploadToPoolDiv").classList.add("hidden");
  document.getElementById("generatePoolDiv").classList.add("hidden");
  document.getElementById("inspectPoolDiv").classList.add("hidden");
}

function uploadPoolPicClick() {

  document.getElementById("newPoolFormDiv").classList.add("hidden");
  document.getElementById("uploadToPoolDiv").classList.remove("hidden");
  document.getElementById("generatePoolDiv").classList.add("hidden");
  document.getElementById("inspectPoolDiv").classList.add("hidden");
}

function generatePoolClick() {

  document.getElementById("newPoolFormDiv").classList.add("hidden");
  document.getElementById("uploadToPoolDiv").classList.add("hidden");
  document.getElementById("generatePoolDiv").classList.remove("hidden");
  document.getElementById("inspectPoolDiv").classList.add("hidden");
}

function inspectPoolsClick() {

  document.getElementById("newPoolFormDiv").classList.add("hidden");
  document.getElementById("uploadToPoolDiv").classList.add("hidden");
  document.getElementById("generatePoolDiv").classList.add("hidden");
  document.getElementById("inspectPoolDiv").classList.remove("hidden");
}

function newMotiveCollClick() {

  document.getElementById("motiveCollDiv").classList.remove("hidden");
  document.getElementById("uploadMotiveCollDiv").classList.add("hidden");
  document.getElementById("motiveBrowser").classList.add("hidden");
}

function uploadMotiveClick() {

  document.getElementById("motiveCollDiv").classList.add("hidden");
  document.getElementById("uploadMotiveCollDiv").classList.remove("hidden");
  document.getElementById("motiveBrowser").classList.add("hidden");
}

function motiveBrowsClick() {

  document.getElementById("motiveCollDiv").classList.add("hidden");
  document.getElementById("uploadMotiveCollDiv").classList.add("hidden");
  document.getElementById("motiveBrowser").classList.remove("hidden");
}

function makeNewMosaicClick() {

  document.getElementById("newMosaicDiv").classList.remove("hidden")
  document.getElementById("browseMosaicsDiv").classList.add("hidden")
}

function browseMosaicsClick() {

  document.getElementById("newMosaicDiv").classList.add("hidden")
  document.getElementById("browseMosaicsDiv").classList.remove("hidden")
}

// ----------------------------------------------------------------------------