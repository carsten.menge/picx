// ----------------------------------------------------------------------------
// imageHandling.go contains the logic for handling the images
// ----------------------------------------------------------------------------
package main

import (
	"errors"
	"image"
	"image/color"
	"image/draw"
	_ "image/gif"
	_ "image/jpeg"
	_ "image/png"
	"math"
	"math/rand"
	"sort"
	"strconv"
	"strings"

	"github.com/disintegration/imaging"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// ----------------------------------------------------------------------------
// functions for the image services

func poolResizeService() {

	var poolPic poolPic
	for {
		poolPic = <-poolPicResizeChan
		resizePoolImage(poolPic)
	}
}

func poolMetaDataService() {

	var poolPic poolPic
	for {
		poolPic = <-poolPicMetaDataChan
		generateMetaData(poolPic)
	}
}

func motiveThumbnailService() {

	var motivePic motivePic
	for {
		motivePic = <-motiveThumbnailChan
		generateMotiveThumbnailAndMetaData(motivePic)
	}
}

func mosaicService() {

	var mosaicRequest mosaicRequest
	for {
		mosaicRequest = <-mosaicChan
		makeMosaic(mosaicRequest.name, mosaicRequest.pool, mosaicRequest.motivePic, mosaicRequest.n)
	}
}

// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// function to resize a poolImage
func resizePoolImage(poolImage poolPic) {

	// open the existing poolImage fromt the gridfs
	gridFile, err := gridfs.OpenId(poolImage.FileID)
	if err != nil {
		errorHandler(err)
	}

	// decode the image
	img, err := imaging.Decode(gridFile, imaging.AutoOrientation(true))
	if err != nil {
		errorHandler(err)
	}

	// replace the seeker to the start of the file
	gridFile.Seek(0, 0)

	// get the image config for width and height
	imgConf, _, err := image.DecodeConfig(gridFile)
	if err != nil {
		errorHandler(err)
	}

	// check if the image already has the correct size and return if true
	if imgConf.Height == poolImage.Size && imgConf.Width == poolImage.Size {
		// only put the image on the meta data queue and return
		poolImage.Processed = true
		poolPicMetaDataChan <- poolImage
		return
	}

	size := intMin(imgConf.Height, imgConf.Width)

	// crop to greatest square, resize to size
	resizedImg := imaging.CropCenter(img, size, size)

	// resize the image
	resizedImg =
		imaging.Resize(resizedImg, poolImage.Size, poolImage.Size, imaging.Linear)

	// get the format from the filename extension
	format, err := getImageFormat(gridFile.Name())
	if err != nil {
		errorHandler(err)
		return
	}

	// create a new gridfile
	newGridFile, err := gridfs.Create(gridFile.Name())
	imaging.Encode(newGridFile, resizedImg, format)

	newGridFile.SetName(renameWithSize(gridFile.Name(), poolImage.Size))

	// replace the picture for the user
	newPoolImage := poolImage
	newPoolImage.ID = bson.NewObjectId()
	newPoolImage.FileID = newGridFile.Id().(bson.ObjectId)
	newPoolImage.Processed = true
	replacePoolPic(poolImage, newPoolImage)

	err = gridFile.Close()
	if err != nil {
		errorHandler(err)
	}

	err = newGridFile.Close()
	if err != nil {
		errorHandler(err)
	}

	// put the new pool image back on the channel for MetaData generation
	poolPicMetaDataChan <- newPoolImage
}

// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// function to generate meta data for poolPics
func generateMetaData(poolPic poolPic) {

	// open the gridfile
	gridFile, err := gridfs.OpenId(poolPic.FileID)
	if err != nil {
		errorHandler(err)
	}

	// decode the image
	img, err := imaging.Decode(gridFile, imaging.AutoOrientation(true))
	if err != nil {
		errorHandler(err)
	}

	// get bounds, prepare some variables and iterate over the pixels
	bounds := img.Bounds()
	var rT, gT, bT, aT, pxCount uint32
	rT, gT, bT, aT = 0, 0, 0, 0
	pxCount = 0

	for y := bounds.Min.Y; y < bounds.Max.Y; y++ {
		for x := bounds.Min.X; x < bounds.Max.X; x++ {

			r, g, b, a := img.At(x, y).RGBA()
			rT += r
			gT += g
			bT += b
			aT += a
			pxCount += 1
		}
	}

	// write the metadata, divide the found color values by pxCount and make sure
	// we get proper values between 0 and 255
	metaData := imageMetaData{}
	metaData.Height = bounds.Max.Y - bounds.Min.Y
	metaData.Width = bounds.Max.X - bounds.Min.X
	metaData.AvgColor = colorStruct{
		int(rT/pxCount) / 256,
		int(gT/pxCount) / 256,
		int(bT/pxCount) / 256,
		int(aT/pxCount) / 256,
	}
	// ObjectIds cant be empty...
	metaData.GradientFileID = bson.NewObjectId()

	poolPic.MetaData = metaData
	poolPic.MetaDataGenerated = true

	// update with the new data
	err = db.C(poolPicCollection).UpdateId(poolPic.ID, poolPic)
	if err != nil {
		errorHandler(err)
	}
}

// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// function to generate metadata and a thumbnail for motivePic
func generateMotiveThumbnailAndMetaData(motivePic motivePic) {

	// open the motive picture on gridfs
	gridFile, err := gridfs.OpenId(motivePic.FileID)
	if err != nil {
		errorHandler(err)
	}

	// create a new gridfile for the gradient picture
	gridFileGradient, err := gridfs.Create(affixFileName(motivePic.Name, "_gradient"))
	if err != nil {
		errorHandler(err)
	}

	gradientFileID := bson.NewObjectId()
	gridFileGradient.SetId(gradientFileID)
	defer gridFileGradient.Close()

	// decode the image
	img, err := imaging.Decode(gridFile, imaging.AutoOrientation(true))
	if err != nil {
		errorHandler(err)
	}

	// generate the gradient picture into the gridFile
	generateGradientForMotive(gridFileGradient, img)

	// get bounds, prepare some variables and iterate over the pixels
	bounds := img.Bounds()
	var rT, gT, bT, pxCount uint32
	rT, gT, bT = 0, 0, 0
	pxCount = 0

	for y := bounds.Min.Y; y <= bounds.Max.Y; y++ {
		for x := bounds.Min.X; x <= bounds.Max.X; x++ {

			r, g, b, _ := img.At(x, y).RGBA()
			rT += r
			gT += g
			bT += b
			pxCount += 1
		}
	}

	// write the metadata
	metaData := imageMetaData{}
	metaData.Height = bounds.Max.Y - bounds.Min.Y
	metaData.Width = bounds.Max.X - bounds.Min.X
	metaData.GradientFileID = gradientFileID

	motivePic.MetaData = metaData
	motivePic.MetaDataGenerated = true

	// create a new file for the thumbnail and affix the filename
	thumbnailgf, err := gridfs.Create(affixFileName(gridFile.Name(), "_thmb"))
	if err != nil {
		errorHandler(err)
	}

	var thumbnail *image.NRGBA

	// resize so, that the biggest dimension fits the thumbnailsize
	if motivePic.MetaData.Height > motivePic.MetaData.Width {
		thumbnail = imaging.Resize(img, thumbnailSize, 0, imaging.Linear)
	} else {
		thumbnail = imaging.Resize(img, 0, thumbnailSize, imaging.Linear)
	}

	// encode the new image as JPEG
	imaging.Encode(thumbnailgf, thumbnail, imaging.JPEG)

	motivePic.ThumbnailID = thumbnailgf.Id().(bson.ObjectId)
	motivePic.ThumbnailGenerated = true

	err = gridFile.Close()
	if err != nil {
		errorHandler(err)
	}

	err = thumbnailgf.Close()
	if err != nil {
		errorHandler(err)
	}

	err = db.C(motivePicCollection).UpdateId(motivePic.ID, motivePic)
	if err != nil {
		errorHandler(err)
	}
}

// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// function to create an artificial pool
// 	for user with userID,
// 	using name,
// 	creating n tiles,
//	of size in px,
// 	starting with startingColor,
//	with ranges for red, green and blue,
//	at noiseLevel in %,
// 	using noiseColor for the noise
func makeArtificialPool(
	userID string,
	name string,
	n int,
	size int,
	startingColor color.Color,
	colorRange []uint8,
	noiseLevel int,
	noiseColor color.Color,
) {

	// add the pool to the user
	id := addPool(userID, name, size)

	// --------------------------------------------------------------------------
	// loop to n
	for i := 0; i < n; i++ {

		// get a random color in range
		drawColor := getRandomColorInRange(startingColor, colorRange)

		// create and draw a new image in the color
		img := image.NewNRGBA(image.Rect(0, 0, size, size))
		draw.Draw(
			img,
			img.Bounds(),
			&image.Uniform{drawColor},
			image.ZP,
			draw.Src,
		)

		// add noise to the image
		img = addNoise(img, float32(noiseLevel)/100, noiseColor)

		// create a gridFile
		gridFile, err := gridfs.Create(name + "_image_" + strconv.Itoa(i) + ".jpg")
		if err != nil {
			errorHandler(err)
		}

		// encode as JPEG
		imaging.Encode(gridFile, img, imaging.JPEG)

		// save by closing the gridFile
		err = gridFile.Close()
		if err != nil {
			errorHandler(err)
		}

		// add the image to the pool
		addToPool(size, id, gridFile.Id().(bson.ObjectId))
	}
	// --------------------------------------------------------------------------
}

// ----------------------------------------------------------------------------
// helper functions

// get the image format from the file name
func getImageFormat(filename string) (imaging.Format, error) {

	split := strings.Split(filename, ".")
	extension := split[len(split)-1]
	extension = strings.ToLower(extension)

	switch extension {
	case "jpg", "jpeg":
		return imaging.JPEG, nil
	case "png":
		return imaging.PNG, nil
	case "gif":
		return imaging.GIF, nil
	case "tiff":
		return imaging.TIFF, nil
	case "bmp":
		return imaging.BMP, nil
	default:
		return -1, errors.New("not a valid image file")
	}
}

// rename a file with the affixed size in px
func renameWithSize(filename string, size int) string {

	sizeStr := "_" + strconv.Itoa(size) + "px"
	return affixFileName(filename, sizeStr)
}

// check wether a file is of a usable image format
func checkImageFormat(filename string) bool {

	split := strings.Split(filename, ".")
	extension := split[len(split)-1]
	extension = strings.ToLower(extension)

	switch extension {
	case "jpg", "jpeg", "png", "gif", "tiff", "bmp":
		return true
	default:
		return false
	}
}

// minimum of two ints
func intMin(x int, y int) int {

	if x < y {
		return x
	} else {
		return y
	}
}

// function to affix a string to the end of a filename
func affixFileName(filename string, affix string) string {

	split := strings.Split(filename, ".")
	extension := split[len(split)-1]
	preExt := strings.Join(split[0:len(split)-1], "")

	return preExt + affix + "." + extension
}

// function to get a random color in a range expressed by a slice for RGB
func getRandomColorInRange(startingColor color.Color, colorRange []uint8) color.Color {

	r, g, b, a := startingColor.RGBA()

	// divide by 16, because RGBA() premultiplies color values
	rN := getRandomColorValue(int(r/16), int(colorRange[0]))
	gN := getRandomColorValue(int(g/16), int(colorRange[1]))
	bN := getRandomColorValue(int(b/16), int(colorRange[2]))

	newColor := color.RGBA{rN, gN, bN, uint8(a)}

	return newColor
}

// function to get a random single new value for a color
func getRandomColorValue(old int, rng int) uint8 {

	rangeToLow := rng
	rangeToHigh := rng
	// first we figure our range
	if old-rng < 0 {
		rangeToLow = rng - old
	}
	if old+rng > 255 {
		rangeToHigh = rng - (old - 255)
	}

	// 50-50 up or down
	coin := rand.Intn(2)
	var newV uint8
	if coin == 0 {
		if rangeToLow <= 0 {
			newV = uint8(old)
		} else {
			newV = uint8(old - rand.Intn(rangeToLow))
		}
	} else {
		if rangeToHigh <= 0 {
			newV = uint8(old)
		} else {
			newV = uint8(old + rand.Intn(rangeToHigh))
		}
	}

	return newV
}

// function to add random noise to a picture
func addNoise(img *image.NRGBA, noiseLevel float32, noiseColor color.Color) *image.NRGBA {

	bounds := img.Bounds()

	for y := bounds.Min.Y; y <= bounds.Max.Y; y++ {
		for x := bounds.Min.X; x <= bounds.Max.X; x++ {

			if rand.Float32() < noiseLevel {
				img.Set(x, y, noiseColor)
			}
		}
	}

	return img
}

// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// function to generate a gradient picture for a pool
// we need to do this every time, because pictures can be added to a pool
func generateGradientForPool(pool pool) image.Image {

	// create a rectangle for the new image
	newBounds := image.Rect(0, 0, len(pool.PicsIDs), 24)

	newImg := image.NewRGBA(newBounds)

	var hsvSlice []HSVColor

	// iterate over the picIDs, get average color and convert to HSV
	for _, picID := range pool.PicsIDs {

		pic := getPoolPicByID(picID)

		col := color.RGBA{
			uint8(pic.MetaData.AvgColor.R),
			uint8(pic.MetaData.AvgColor.G),
			uint8(pic.MetaData.AvgColor.B),
			uint8(pic.MetaData.AvgColor.A),
		}

		hsvSlice = append(hsvSlice, convertToHSV(col))
	}

	// sort by hue
	sort.Sort(byHue(hsvSlice))

	// iterate over the slice to draw the new picture
	for i, hsvCol := range hsvSlice {

		stripeBounds := image.Rect(i, 0, (i)+1, 24)

		draw.Draw(
			newImg,
			stripeBounds,
			&image.Uniform{convertToRGB(hsvCol)},
			image.ZP,
			draw.Src,
		)
	}

	// return the picture
	return newImg
}

// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// function to generate a gradient picture for a motive
func generateGradientForMotive(gridFile *mgo.GridFile, img image.Image) {

	// first resize the image to something easily manageable
	resizedImg := imaging.Resize(img, 20, 0, imaging.Linear)

	var colSlice []HSVColor

	// iterate over the resized image
	bounds := resizedImg.Bounds()
	for y := bounds.Min.Y; y <= bounds.Max.Y; y++ {
		for x := bounds.Min.X; x <= bounds.Max.X; x++ {

			col := convertToHSV(resizedImg.At(x, y))
			colSlice = append(colSlice, col)
		}
	}

	// sort the slice by hue
	sort.Sort(byHue(colSlice))

	// now we make a new image from the sorted colors
	newBounds := image.Rect(0, 0, len(colSlice), 24)

	newImg := image.NewRGBA(newBounds)

	// range over the slice and add a stripe for every color
	for i, hsvColor := range colSlice {

		stripeBounds := image.Rect(i, 0, (i)+1, 24)

		col := convertToRGB(hsvColor)
		draw.Draw(
			newImg,
			stripeBounds,
			&image.Uniform{col},
			image.ZP,
			draw.Src,
		)
	}

	imaging.Encode(gridFile, newImg, imaging.JPEG)

}

// ----------------------------------------------------------------------------
// function to convert a RGB color to HSV. For the algorithm, see
// https://de.wikipedia.org/wiki/HSV-Farbraum
func convertToHSV(col color.Color) HSVColor {

	r1, g1, b1, _ := col.RGBA()
	var r, g, b float64

	// convert to float 0 - 1
	r = float64(r1) / 65025
	g = float64(g1) / 65025
	b = float64(b1) / 65025

	max := maxThree(r, g, b)
	min := minThree(r, g, b)

	var h, s, v float64

	if r == g && g == b {
		h = 0
	} else if max == r {
		h = 60 * (0 + ((g-b)/max - min))
	} else if max == g {
		h = 60 * (2 + ((b-r)/max - min))
	} else if max == b {
		h = 60 * (4 + ((r-g)/max - min))
	}

	if h < 0 {
		h = h + 360
	}

	if max == 0 {
		s = 0
	} else {
		s = (max - min) / max
	}

	v = max

	return HSVColor{h, s, v}
}

// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// function to convert a HSV color to RGB. For the algorithm, see
// https://de.wikipedia.org/wiki/HSV-Farbraum
func convertToRGB(col HSVColor) color.Color {

	var r, g, b float64

	hi := int(col.H / 60)

	f := (col.H/60 - float64(hi))

	p := col.V * (1 - col.S)

	q := col.V * (1 - col.S*f)

	t := col.V * (1 - col.S*(1-f))

	switch hi {
	case 0, 6:
		r, g, b = col.V, t, p
	case 1:
		r, g, b = q, col.V, p
	case 2:
		r, g, b = p, col.V, t
	case 3:
		r, g, b = p, q, col.V
	case 4:
		r, g, b = t, p, col.V
	case 5:
		r, g, b = col.V, p, q
	}

	return color.RGBA{
		uint8(math.Round(r * 255)),
		uint8(math.Round(g * 255)),
		uint8(math.Round(b * 255)),
		255,
	}
}

// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// helper functions to get the maximum of three floats
func maxThree(a, b, c float64) float64 {

	if a > b && a > c {
		return a
	} else if b > a && b > c {
		return b
	} else {
		return c
	}
}

// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// helper functions to get the minimum of three floats
func minThree(a, b, c float64) float64 {

	if a < b && a < c {
		return a
	} else if b < a && b < c {
		return b
	} else {
		return c
	}
}

// ----------------------------------------------------------------------------
