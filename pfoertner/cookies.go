package pfoertner

import (
	"errors"
	"net/http"
	"time"

	"gopkg.in/mgo.v2/bson"
)

// RequestCookie requests a login-cookie. Takes a userName and password and returns a cookie
// response object and an error. The returned cookie is empty if there's an error.
// Errors are returned for non-existant users and wrong passwords.
func (s *Session) RequestCookie(userName string, password string) (http.Cookie, error) {

	requestData := cookieRequest{
		userName,
		password,
	}

	response, userExists, pwCorrect := s.createCookieResponse(requestData)

	cookie := http.Cookie{
		Name:    response.UserName,
		Value:   response.IDstring,
		Expires: response.Expires,
	}

	if !userExists {
		return cookie, errors.New("username does not exist")
	} else if !pwCorrect {
		return cookie, errors.New("password incorrect")
	} else {
		return cookie, nil
	}

}

// createCookieResponse populates the cookieResponse object and returns wether password and username are correct
func (s *Session) createCookieResponse(requestData cookieRequest) (response cookieResponse, userExists bool, pwCorrect bool) {

	user := UserRead{}
	response = cookieResponse{}

	if !s.userNameExists(requestData.UserName) {
		response.UserName = ""
		response.IDstring = ""
		userExists = false
	} else {
		userExists = true
		s.userCollection.Find(bson.M{"UserName": requestData.UserName}).One(&user)

		if user.Password == requestData.Password {
			response.UserName = requestData.UserName
			response.IDstring = user.ID.Hex()
			response.Expires = time.Now().Add(time.Hour * expiryTime)
			pwCorrect = true
		} else {
			response.UserName = requestData.UserName
			response.IDstring = ""
			pwCorrect = false
		}
	}
	return response, userExists, pwCorrect
}

// CheckCookie takes a cookie string and checks its validity.
func (s *Session) CheckCookie(cookie http.Cookie) bool {

	if cookie.Name == "" {
		return false
	} else if cookie.Value == "" {
		return false
	} else {
		// Find the user in the db
		user := UserRead{}
		s.userCollection.Find(bson.M{"UserName": cookie.Name}).One(&user)

		if user.ID.Hex() == cookie.Value {
			return true
		}

		return false
	}
}
