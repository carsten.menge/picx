// ----------------------------------------------------------------------------
// main.go contains all http handling
// TODO: more indepth comments
// ----------------------------------------------------------------------------

package main

import (
	"encoding/json"
	"fmt"
	"html/template"
	"image/color"
	"io"
	"math/rand"
	"net/http"
	"runtime"
	"strconv"
	"strings"
	"time"

	"github.com/disintegration/imaging"
	"gitlab.com/carsten.menge/picx/pfoertner"
	"gopkg.in/mgo.v2/bson"
)

// ----------------------------------------------------------------------------
// entry point. connecting services, registering handlers, starting
// the server
func main() {

	// --------------------------------------------------------------------------
	// set a seed for random number generation
	rand.Seed(time.Now().UnixNano())
	// --------------------------------------------------------------------------

	// --------------------------------------------------------------------------
	// connect to database
	db = connectDB()
	gridfs = db.GridFS("files")
	p = pfoertner.New(dbAddress, dbName)
	// --------------------------------------------------------------------------

	// --------------------------------------------------------------------------
	// start the imageHandling services
	go poolResizeService()
	go poolMetaDataService()
	go motiveThumbnailService()
	go mosaicService()
	// --------------------------------------------------------------------------

	// --------------------------------------------------------------------------
	// register http handlers
	// site navigation handlers
	http.HandleFunc("/picx", mainHandler)
	http.HandleFunc("/picx/pool", poolNavHandler)
	http.HandleFunc("/picx/motives", motivesNavHandler)
	http.HandleFunc("/picx/mosaics", mosaicsHandler)

	// user management handlers
	http.HandleFunc("/picx/login", loginHandler)
	http.HandleFunc("/picx/logout", logoutHandler)
	http.HandleFunc("/picx/register", registerUser)
	http.HandleFunc("/picx/backToLogin", toLoginHandler)

	// add things
	http.HandleFunc("/picx/addPool", addPoolHandler)
	http.HandleFunc("/picx/addMotivesColl", addMotivesCollHandler)
	http.HandleFunc("/picx/uploadToPool", uploadPoolHandler)
	http.HandleFunc("/picx/uploadToMotives", uploadMotivesHandler)
	http.HandleFunc("/picx/makePool", makePoolHandler)

	// read things
	http.HandleFunc("/picx/getMotiveColls", getMotiveCollsHandler)
	http.HandleFunc("/picx/getMotiveColl", getMotiveCollHandler)
	http.HandleFunc("/picx/getMotive", getMotiveHandler)
	http.HandleFunc("/picx/getImage", getImageHandler)
	http.HandleFunc("/picx/getMosaics", getMosaicsHandler)
	http.HandleFunc("/picx/getPool", getPoolHandler)
	http.HandleFunc("/picx/getPoolPic", getPoolPicHandler)
	http.HandleFunc("/picx/getPoolGradient", getPoolGradientHandler)

	// make things :)
	http.HandleFunc("/picx/makeMosaic", makeMosaicHandler)
	// --------------------------------------------------------------------------

	// --------------------------------------------------------------------------
	// initialise empty pageData
	pageData.LoggedIn = false
	pageData.Register = false
	pageData.UserID = ""
	pageData.Msg = ""
	pageData.Pages = Pages
	pageData.CurrentPage = Pages["LandingPage"]
	// --------------------------------------------------------------------------

	// --------------------------------------------------------------------------
	// serve static files
	fs := http.FileServer(http.Dir("statics"))
	http.Handle("/statics/", http.StripPrefix("/statics/", fs))
	// --------------------------------------------------------------------------

	// --------------------------------------------------------------------------
	// start server
	err := http.ListenAndServe(":4242", nil)
	if err != nil {
		errorHandler(err)
	}
	// --------------------------------------------------------------------------
}

// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// handler functions

// mainHandler for serving up the templates
func mainHandler(w http.ResponseWriter, r *http.Request) {

	// Checking for logged in User
	if pageData.LoggedIn && len(r.Cookies()) > 0 {
		cookie := r.Cookies()[0]
		if p.CheckCookie(*cookie) {
			pageData.UserID = cookie.Value
			pageData.LoggedIn = true
			// update Pools and MotiveColls
			pageData.Pools = getUserPools(pageData.UserID)
			pageData.MotiveColls = getUserMotiveColls(pageData.UserID)
		}
	}

	t := template.Must(template.ParseFiles(mainPage, loginTmpl, logoutTmpl, registerTmpl, motiveUploadTmpl, mosaicsTmpl, poolUploadTmpl, landingPageTmpl, errorPageTmpl))
	t.ExecuteTemplate(w, "main", pageData)
}

// navigation handler to MotivesUpload
func motivesNavHandler(w http.ResponseWriter, r *http.Request) {

	if len(r.Cookies()) > 0 && p.CheckCookie(*r.Cookies()[0]) {
		pageData.CurrentPage = pageData.Pages["MotivesUpload"]
		mainHandler(w, r)
	} else {
		pageData.Msg = "Kein User eingeloggt!"
		mainHandler(w, r)
	}
}

// navigation handler to PoolUpload
func poolNavHandler(w http.ResponseWriter, r *http.Request) {

	if len(r.Cookies()) > 0 && p.CheckCookie(*r.Cookies()[0]) {
		pageData.CurrentPage = pageData.Pages["PoolUpload"]
		mainHandler(w, r)
	} else {
		pageData.Msg = "Kein User eingeloggt!"
		mainHandler(w, r)
	}
}

// navigation handler to PictureBrowser
func mosaicsHandler(w http.ResponseWriter, r *http.Request) {

	if len(r.Cookies()) > 0 && p.CheckCookie(*r.Cookies()[0]) {
		pageData.CurrentPage = pageData.Pages["Mosaics"]
		mainHandler(w, r)
	} else {
		pageData.Msg = "Kein User eingeloggt!"
		mainHandler(w, r)
	}
}

// handler to add new pool
func addPoolHandler(w http.ResponseWriter, r *http.Request) {

	size, err := strconv.Atoi(r.FormValue("newPoolSize"))
	if err != nil {
		errorHandler(err)
	}

	addPool(pageData.UserID, r.FormValue("newPoolName"), size)
	mainHandler(w, r)
}

// handler to add new motives collection
func addMotivesCollHandler(w http.ResponseWriter, r *http.Request) {

	addMotivesCollection(pageData.UserID, r.FormValue("motiveCollName"))
	mainHandler(w, r)
}

// handler to make artificial pools
func makePoolHandler(w http.ResponseWriter, r *http.Request) {

	// TODO: Add Error Handling
	userID := pageData.UserID
	name := r.FormValue("poolName")
	poolLength, _ := strconv.Atoi(r.FormValue("poolLength"))
	poolSize, _ := strconv.Atoi(r.FormValue("poolSize"))
	startingColorString := r.FormValue("startingColor")
	startingColor := convertHexToColor(startingColorString)
	colorRangeR, _ := strconv.Atoi(r.FormValue("colorRangeR"))
	colorRangeG, _ := strconv.Atoi(r.FormValue("colorRangeG"))
	colorRangeB, _ := strconv.Atoi(r.FormValue("colorRangeB"))
	noiseLevel, _ := strconv.Atoi(r.FormValue("noiseLevel"))
	noiseColorString := r.FormValue("noiseColor")
	noiseColor := convertHexToColor(noiseColorString)

	colorRange := []uint8{
		uint8(colorRangeR),
		uint8(colorRangeG),
		uint8(colorRangeB),
	}

	makeArtificialPool(
		userID,
		name,
		poolLength,
		poolSize,
		startingColor,
		colorRange,
		noiseLevel,
		noiseColor,
	)

	mainHandler(w, r)
}

// handler for uploading to a pool
func uploadPoolHandler(w http.ResponseWriter, r *http.Request) {

	poolSize := getPool(bson.ObjectIdHex(r.FormValue("pools"))).Size

	fileIDs, err1 := saveToGridFs(w, r)
	if err1 != nil {
		errorHandler(err1)
	}

	poolID := bson.ObjectIdHex(r.FormValue("pools"))
	for _, fileID := range fileIDs {
		addToPool(poolSize, poolID, fileID)
	}

	mainHandler(w, r)
}

// handler to upload to a motives collection
func uploadMotivesHandler(w http.ResponseWriter, r *http.Request) {

	fileIDs, err1 := saveToGridFs(w, r)
	if err1 != nil {
		errorHandler(err1)
	}

	motiveCollID := bson.ObjectIdHex(r.FormValue("motiveColl"))
	name := r.FormValue("motiveName")
	for _, fileID := range fileIDs {
		addToMotivesCollection(name, motiveCollID, fileID.Hex())
	}

	mainHandler(w, r)
}

// handler to get a pool by ID
func getPoolHandler(w http.ResponseWriter, r *http.Request) {

	poolID := r.URL.Query()["poolID"][0]

	pool := getPool(bson.ObjectIdHex(poolID))

	err := json.NewEncoder(w).Encode(pool)
	if err != nil {
		errorHandler(err)
	}
}

// handler to get a poolPicture by ID
func getPoolPicHandler(w http.ResponseWriter, r *http.Request) {

	picID := r.URL.Query()["poolPicID"][0]

	pic := getPoolPicByID(bson.ObjectIdHex(picID))

	err := json.NewEncoder(w).Encode(pic)
	if err != nil {
		errorHandler(err)
	}
}

// handler to get a motives collection
func getMotiveCollsHandler(w http.ResponseWriter, r *http.Request) {

	userID := r.URL.Query()["userID"][0]
	user := getUser(userID)

	var collections []motiveCollResponse

	for _, motiveCollID := range user.MotiveCollsIDs {

		collection := getMotiveColl(motiveCollID)
		var collectionRes motiveCollResponse

		var motivePics []motivePic
		for _, motivePicID := range collection.MotivePicsIDs {
			motivePics = append(
				motivePics,
				getMotivePicByID(motivePicID),
			)
		}
		collectionRes.ID = collection.ID
		collectionRes.IDHex = collection.IDHex
		collectionRes.Name = collection.Name
		collectionRes.UserID = collection.UserID
		collectionRes.MotivePics = motivePics

		collections = append(
			collections,
			collectionRes,
		)

	}

	err := json.NewEncoder(w).Encode(collections)
	if err != nil {
		errorHandler(err)
	}
}

func getMotiveCollHandler(w http.ResponseWriter, r *http.Request) {

	motiveCollID := r.URL.Query()["motiveCollID"][0]
	motiveColl := getMotiveColl(bson.ObjectIdHex(motiveCollID))

	err := json.NewEncoder(w).Encode(motiveColl)
	if err != nil {
		errorHandler(err)
	}
}

func getMotiveHandler(w http.ResponseWriter, r *http.Request) {

	motiveID := r.URL.Query()["motiveID"][0]
	motivePic := getMotivePicByID(bson.ObjectIdHex(motiveID))

	err := json.NewEncoder(w).Encode(motivePic)
	if err != nil {
		errorHandler(err)
	}
}

func getMosaicsHandler(w http.ResponseWriter, r *http.Request) {

	userID := r.URL.Query()["userID"][0]
	user := getUser(userID)

	var mosaics []mosaicPic
	for _, mosaicID := range user.MosaicsIDs {
		mosaics = append(mosaics, getMosaic(mosaicID))
	}

	err := json.NewEncoder(w).Encode(mosaics)
	if err != nil {
		errorHandler(err)
	}
}

// handler to get an image file from gridfs
func getImageHandler(w http.ResponseWriter, r *http.Request) {

	pictureFileID := r.URL.Query()["id"][0]

	gridFile, err := gridfs.OpenId(bson.ObjectIdHex(pictureFileID))
	if err != nil {
		errorHandler(err)
		return
	}

	// set the header to the correct MIME type
	w.Header().Set("Content-Type", getFileMIMETypeString(gridFile.Name()))
	_, err = io.Copy(w, gridFile)
	if err != nil {
		errorHandler(err)
	}

	err = gridFile.Close()
	if err != nil {
		errorHandler(err)
	}
}

func getPoolGradientHandler(w http.ResponseWriter, r *http.Request) {

	poolID := r.URL.Query()["poolID"][0]
	img := generateGradientForPool(getPool(bson.ObjectIdHex(poolID)))

	// set the header to the correct MIME type
	w.Header().Set("Content-Type", "image/jpeg")
	err := imaging.Encode(w, img, imaging.JPEG)
	if err != nil {
		errorHandler(err)
	}

}

// handler to do the stuff we're all actually here for
func makeMosaicHandler(w http.ResponseWriter, r *http.Request) {

	motivePicID := r.FormValue("mosaicMotivePicID")
	poolID := r.FormValue("pool")
	n, _ := strconv.Atoi(r.FormValue("n"))

	mosaicRequest := mosaicRequest{}
	mosaicRequest.name = r.FormValue("mosaicName")
	mosaicRequest.motivePic = getMotivePicByID(bson.ObjectIdHex(motivePicID))
	mosaicRequest.pool = getPool(bson.ObjectIdHex(poolID))
	mosaicRequest.n = n

	mosaicChan <- mosaicRequest

	mainHandler(w, r)
}

// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// helper functions

// function to get the MIME type from a filename extension
func getFileMIMETypeString(filename string) string {

	tmpSlice := strings.Split(filename, ".")
	fileExtension := tmpSlice[len(tmpSlice)-1] // das letzte Element
	fileExtension = strings.ToLower(fileExtension)

	switch fileExtension {
	case "jpeg", "jpg":
		return "image/jpeg"
	case "png":
		return "image/png"
	case "gif":
		return "image/gif"
	case "tiff":
		return "image/tiff"
	case "bmp":
		return "image/bmp"
	default:
		return "text/html"
	}
}

// convert a hex RGB value to Color
func convertHexToColor(hexString string) color.Color {

	slice := strings.Split(hexString, "")
	rString := strings.Join(slice[1:3], "")
	gString := strings.Join(slice[3:5], "") // höhö
	bString := strings.Join(slice[5:7], "")

	fmt.Printf("string: %v, r:%v, g:%v, b%v\n", hexString, rString, gString, bString)

	// Todo: Add Error handlinc
	r, _ := strconv.ParseUint(rString, 16, 64)
	g, _ := strconv.ParseUint(gString, 16, 64)
	b, _ := strconv.ParseUint(bString, 16, 64)
	fmt.Printf("r:%v, g:%v, b:%v\n", r, g, b)

	return color.RGBA{uint8(r), uint8(g), uint8(b), 255}
}

// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// generic error handler
func errorHandler(err error) {

	fpcs := make([]uintptr, 1)
	// Skip 2 levels to get the caller
	n := runtime.Callers(2, fpcs)
	if n == 0 {
		fmt.Println("MSG: NO CALLER")
	}

	caller := runtime.FuncForPC(fpcs[0] - 1)
	if caller == nil {
		fmt.Println("MSG CALLER WAS NIL")
	}

	// get the file name and line number
	callerFileName, callerLnNumber := caller.FileLine(fpcs[0] - 1)

	// get the name of the function
	callerFncName := caller.Name()

	fmt.Printf("%s, ln: %d %s: %s", callerFileName, callerLnNumber-2, callerFncName, err.Error())
}

// ----------------------------------------------------------------------------
