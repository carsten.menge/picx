// ----------------------------------------------------------------------------
// data.go contains data structures and global variables
// ----------------------------------------------------------------------------

package main

import (
	"gitlab.com/carsten.menge/picx/pfoertner"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// ----------------------------------------------------------------------------
// types
type pageDataStruct struct {
	LoggedIn    bool
	Register    bool
	UserID      string
	UserName    string
	Msg         string
	CurrentPage int
	Pages       map[string]int
	MotiveColls []motiveColl
	Pools       []pool
}

// database access structs
type mosaicPic struct {
	ID     bson.ObjectId `bson:"_id,omitempty" json:"ID,omitempty"`
	Name   string        `bson:"Name" json:"Name"`
	Done   bool          `bson:"Done" json:"Done"`
	UserID bson.ObjectId `bson:"UserID" json:"UserID"`
	FileID bson.ObjectId `bson:"FileID" json:"FileID"`
}

type colorStruct struct {
	R int `bson:"R" json:"R"`
	B int `bson:"B" json:"B"`
	G int `bson:"G" json:"G"`
	A int `bson:"A" json:"A"`
}

type imageMetaData struct {
	Height         int           `bson:"Height" json:"Height"`
	Width          int           `bson:"Width" json:"Width"`
	AvgColor       colorStruct   `bson:"AvgColor" json:"AvgColor"`
	GradientFileID bson.ObjectId `bson:"GradientFileID" json:"GradientFileID"`
}

type poolPic struct {
	ID                bson.ObjectId `bson:"_id,omitempty" json:"ID,omitempty"`
	IDHex             string        `bson:"IDHex" json:"IDHex"`
	Processed         bool          `bson:"Processed" json:"Processed"`
	MetaDataGenerated bool          `bson:"MetaDataGenerated" json:"MetaDataGenerated"`
	MetaData          imageMetaData `bson:"MetaData" json:"MetaData"`
	Size              int           `bson:"Size" json:"Size"`
	CollectionID      bson.ObjectId `bson:"CollectionID" json:"CollectionID"`
	FileID            bson.ObjectId `bson:"FileID" json:"FileID"`
}

type pool struct {
	ID      bson.ObjectId   `bson:"_id,omitempty" json:"ID,omitempty"`
	IDHex   string          `bson:"IDHex" json:"IDHex"`
	Name    string          `bson:"Name" json:"Name"`
	Size    int             `bson:"Size" json:"Size"`
	UserID  bson.ObjectId   `bson:"UserID" json:"UserID"`
	PicsIDs []bson.ObjectId `bson:"PicsIDs" json:"PicsIDs"`
}

type motivePic struct {
	ID                 bson.ObjectId `bson:"_id,omitempty" json:"ID,omitempty"`
	IDHex              string        `bson:"IDHex" json:"IDHex"`
	Name               string        `bson:"Name" json:"Name"`
	MetaDataGenerated  bool          `bson:"MetaDataGenerated" json:"MetaDataGenerated"`
	MetaData           imageMetaData `bson:"MetaData" json:"MetaData"`
	ThumbnailGenerated bool          `bson:"ThumbnailGenerated" json:"ThumbnailGenerated"`
	ThumbnailID        bson.ObjectId `bson:"ThumbnailID" json:"ThumbnailID"`
	CollectionID       bson.ObjectId `bson:"CollectionID" json:"CollectionID"`
	FileID             bson.ObjectId `bson:"FileID" json:"FileID"`
}

type motiveColl struct {
	ID            bson.ObjectId   `bson:"_id,omitempty" json:"ID,omitempty"`
	IDHex         string          `bson:"IDHex" json:"IDHex"`
	Name          string          `bson:"Name" json:"Name"`
	UserID        bson.ObjectId   `bson:"UserID" json:"UserID"`
	MotivePicsIDs []bson.ObjectId `bson:"MotivePicsIDs" json:"MotivePicsIDs"`
}

type motiveCollResponse struct {
	ID         bson.ObjectId `bson:"_id,omitempty" json:"ID,omitempty"`
	IDHex      string        `bson:"IDHex" json:"IDHex"`
	Name       string        `bson:"Name" json:"Name"`
	UserID     bson.ObjectId `bson:"UserID" json:"UserID"`
	MotivePics []motivePic   `bson:"MotivePics" json:"MotivePics"`
}

type user struct {
	ID             bson.ObjectId   `bson:"_id" json:"ID"`
	IDHex          string          `bson:"IDHex" json:"IDHex"`
	MotiveCollsIDs []bson.ObjectId `bson:"MotiveCollsIDs" json:"MotiveCollsIDs"`
	PoolIDs        []bson.ObjectId `bson:"PoolIDs" json:"PoolIDs"`
	MosaicsIDs     []bson.ObjectId `bson:"MosaicsIDs" json:"MosaicsIDs"`
}

type mosaicRequest struct {
	name      string
	motivePic motivePic
	pool      pool
	n         int
}

// HSV color and sorting
type HSVColor struct {
	H, S, V float64
}

type byHue []HSVColor

func (c byHue) Len() int {
	return len(c)
}

func (c byHue) Swap(i, j int) {
	c[i], c[j] = c[j], c[i]
}

func (c byHue) Less(i, j int) bool {
	return c[i].H < c[j].H
}

// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// map of pages
var Pages = map[string]int{
	"LandingPage":   0,
	"MotivesUpload": 1,
	"PoolUpload":    2,
	"Mosaics":       3,
}

// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// global variables
// session variables
var p *pfoertner.Session
var db *mgo.Database
var gridfs *mgo.GridFS

// template file variables
var mainPage = "./templates/mainPage.html"
var loginTmpl = "./templates/login.html"
var logoutTmpl = "./templates/logout.html"
var registerTmpl = "./templates/register.html"
var motiveUploadTmpl = "./templates/motiveUpload.html"
var mosaicsTmpl = "./templates/mosaics.html"
var poolUploadTmpl = "./templates/poolUpload.html"
var landingPageTmpl = "./templates/landingPage.html"
var errorPageTmpl = "./templates/errorPage.html"

// pageData
var pageData = pageDataStruct{}

// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// constants
const (
	maxConcurrent = 64
	thumbnailSize = 64
)

// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// channels for the image handling services
var poolPicResizeChan = make(chan poolPic, maxConcurrent)
var poolPicMetaDataChan = make(chan poolPic, maxConcurrent)
var motiveThumbnailChan = make(chan motivePic, maxConcurrent)
var mosaicChan = make(chan mosaicRequest, maxConcurrent)

// ----------------------------------------------------------------------------
